import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import { HeaderCount } from './components/HeaderCount/headerCount';
import { LangSelect } from './components/LangSelect/langSelect';
import { RangeSelect } from './components/RangeSelect/rangeSelect';
import { NumbersDisplay } from './components/NumbersDisplay/numbersDisplay';
import { PracticeBlock } from './components/PracticeBlock/PracticeBlock';
import { ChallengeBlock } from './components/ChallengeBlock/ChallengeBlock';

// global arrays
let correctAdd = [];
let incorrectAdd = [];
let resultAdd = [];

let theNum = [];
let maxNum = 100;

let fromList = [1, 21, 41, 61, 81, 101, 200];
let toList = []; //will update

let i;

// the from numbers this never changes
// for (i = 1; i <= maxNum; i += 19) {
//   fromList.push(i);
// }
// the to numbers --Default

for (i = fromList[1]; i <= maxNum; i += 20) {
  toList.push(i);
}
// the displayed numbers --Default
for (i = fromList[0]; i <= toList[0]; i++) {
  theNum.push([i, 'num']);
}

const langs = [
  ['Chinese', 'zh-CN', '中文', '不正确'],
  ['Dutch', 'nl-NL', 'Nederlands', 'onjuist'],
  ['English (GB)', 'en-GB', 'English', 'incorrect'],
  ['English (US)', 'en-US', 'English', 'incorrect'],
  ['French', 'fr-FR', 'français', 'incorrect'],
  ['French (CA)', 'fr-CA', 'français', 'incorrect'],
  ['German', 'de-DE', 'deutsch', 'falsch'],
  ['Hebrew', 'he-IL', 'עִברִית', 'לֹא נָכוֹן'],
  ['Italian', 'it-IT', 'italiano', 'scorretto'],
  ['Japanese', 'ja-JP', '日本語', '間違った'],
  ['Korean', 'ko-KR', '한국어', '부정확 한'],
  ['Polish', 'pl-PL', 'Polskie', 'błędny'],
  ['Portuguese', 'pt-PT', 'Português', 'incorreta'],
  ['Portuguese (BR)', 'pt-BR', 'Português', 'incorreta'],
  ['Russian', 'ru-RU', 'русский', 'некорректный'],
  ['Spanish', 'es-ES', 'Español', 'incorrecto'],
  ['Spanish (MX)', 'es-MX', 'Español', 'incorrecto'],
  ['Swedish', 'sv-SE', 'svenska', 'felaktig'],
  ['Thai', 'th-TH', 'ไทย', 'ไม่ถูกต้อง']
];

// make speak
const speech = window.speechSynthesis;
let to_speak;

function speakDe (say, lang) {
  shutUp();
  to_speak = new SpeechSynthesisUtterance(say);
  to_speak.lang = lang;
  to_speak.rate = 0.9;
  speech.speak(to_speak)
}

function speakDeAll(say, lang) {
  to_speak = new SpeechSynthesisUtterance(say);
  to_speak.lang = lang;
  to_speak.rate = 0.9;
  speech.speak(to_speak)
}

function shutUp() {
  speech.cancel()
}

class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      langs, // the array of languages to select
      langSelected: 6, // sets default language
      lang: "de-DE", // ......
      // num rangeselect
      from: fromList[0],
      to: 20,
      toList,
      theNum, // the array of number to display

      challenge: false, // sets challenge to not started
      // for quiz
      quiz: false,
      finished: false,
      results: [],
      running: false,
      elapsedTime: 0,
      previousTime: 0,

      rand: '', // the random number said
      correct: [], // #endregion//array for correct answers
      incorrect: [], // array for incorrect answers
      incorrectQ: []
    }

    this.handleClick = this.handleClick.bind(this);
    this.handleButtonClick = this.handleButtonClick.bind(this);
    this.handleQuizButtonClick = this.handleQuizButtonClick.bind(this);
    this.onTick = this.onTick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleToChange = this.handleToChange.bind(this);
    this.handleFromChange = this.handleFromChange.bind(this);

  }

  // lang selector
  handleChange (e) {
    let index = e.target.selectedIndex;
    correctAdd = [];
    incorrectAdd = [];
    // resets numbers to remove correct class
    let theNewNum = [];
    for (i = this.state.from; i <= this.state.to; i++) {
      theNewNum.push([i, 'num'])
    }

    this.setState({
      lang: e.target.value,
      langSelected: index,
      theNum: theNewNum,
      correct: [],
      incorrect: [],
      challenge: false,
      incorrectQ: [],
      quiz: false,
      running: false,
      elapsedTime: 0
    });
    speakDe(langs[index][2], e.target.value);
  }
  // From changed
  handleFromChange(e) {
    correctAdd = [];
    incorrectAdd = [];
    // set the min number in the toList
    let newMinTo = Number(e.target.value) + 20;
    // set the max number in the toList
    let newMaxTo = (Number(e.target.value) + 100)
    let newToList = [];
    let theNewNum = [];
    let thisTo;
    //the to numbers -- this needs to change depending on start value
    for (i = newMinTo; i <= newMaxTo; i += 19) {
      newToList.push([i]);
    }
    if (newMinTo >= this.state.to) {
      thisTo = newMinTo;
    } else if (this.state.to > newMaxTo) {
      thisTo = newMaxTo
    } else {
      thisTo = this.state.to;
    }
    // make the array of numbers that are displayed
    for (i = Number(e.target.value); i <= thisTo; i++) {
      theNewNum.push([i, 'num']);
    }

    this.setState({
      from: Number(e.target.value),
      to: Number(thisTo),
      toList: newToList,
      theNum: theNewNum,
      correct: [],
      incorrect: [],
      challenge: false,
      incorrectQ: [],
      quiz: false,
      running: false,
      elapsedTime: 0
    })
  }

  // To changed
  handleToChange(e) {
    correctAdd = [];
    incorrectAdd = [];
    let newTo = Number(e.target.value);
    let theNewNum = [];
    for (i = this.state.from; i <= newTo; i++) {
      theNewNum.push([i, 'num']);
    }
    this.setState({
      to: newTo,
      theNum: theNewNum,
      correct: [],
      incorrect: [],
      challenge: false,
      incorrectQ: [],
      quiz: false,
      running: false,
      elapsedTime: 0
    })
  }

  //number clicked
  handleClick(e) {
    let clicked = Number(e.currentTarget.textContent);
    let lang = this.state.lang;
    let rand = this.state.theNum[Math.floor((Math.random() * this.state.theNum.length))][0];
    let theNum = this.state.theNum;
    let theNewNum = [];
    let numClassC = 'num correct';

    this.setState({ clicked: clicked });


    //challenge is correct
    if (clicked === this.state.rand && this.state.test) {
      correctAdd.push(this.state.rand);
      this.setState({ correct: correctAdd, rand: rand });

      speakDe(clicked, lang);
      speakDe(rand, lang);

      //quiz is correct
    } else if (clicked === this.state.rand && this.state.quiz) {
      //updates the displayed number class
      for (i = 0; i < theNum.length; i++) {
        if (clicked === theNum[i][0]) {
          theNewNum.push([theNum[i][0], numClassC]);
        } else {
          theNewNum.push([theNum[i][0], theNum[i][1]]);
        }
      }

      //make an array of numbers that are not correct
      let forRand = [];
      for (i = 0; i < theNewNum.length; i++) {
        if (theNewNum[i][1] === "num") {
          forRand.push(theNewNum[i][0]);
        }
      }
      let rand2 = forRand[Math.floor((Math.random() * forRand.length))];

      this.setState({ rand: rand2, theNum: theNewNum });

      //checks to if finished
      if (forRand.length < 1) { //NOTE: change for testing
        let language = this.state.langs[this.state.langSelected][0];
        let numbers = this.state.from + ' - ' + this.state.to;
        let timer = Math.floor(this.state.elapsedTime);
        let mins = Math.floor(timer / 60000);
        let secs = ((timer % 60000) / 1000).toFixed(0);
        let theTime = (secs === 60 ? (mins + 1) + ":00" : mins + ":" + (secs < 10 ? "0" : "") + secs);
        let incorrectCount = incorrectAdd.length;
        //add results
        resultAdd.unshift([language, numbers, theTime, incorrectCount]);

        this.setState({ quiz: false, running: false, finished: true, results: resultAdd });
        incorrectAdd = [];
      } else {
        speakDe(rand2, lang);
      }

      //challenge incorrect
    } else if (this.state.test) {
      incorrectAdd.push(this.state.rand);
      this.setState({ incorrect: incorrectAdd, rand: rand });
      //  speakDe(clicked+"  "+rand, lang);
      speakDe(clicked, lang);
      speakDeAll(rand, lang);

      //quiz incorrect
    } else if (this.state.quiz) {
      incorrectAdd.push(this.state.rand);
      this.setState({ incorrectQ: incorrectAdd, rand: rand });
      speakDe(this.state.langs[this.state.langSelected][3], lang);
      speakDeAll(rand, lang);
    } else {
      speakDe(clicked, lang);
    }
  }

  //start practice button
  handleButtonClick(e) {
    let lang = this.state.lang;
    let rand = this.state.theNum[Math.floor((Math.random() * this.state.theNum.length))][0];
    let theNewNum = [];
    for (i = this.state.from; i <= this.state.to; i++) {
      theNewNum.push([i, 'num']);
    }
    // starts practice, resets correct and incorrect arrays
    if (this.state.test !== true) {
      this.setState({ rand: rand, test: true, incorrect: [], correct: [], theNum: theNewNum });
      speakDe(rand, lang);
      // stops test, resets correctAdd and incorrectAdd arrays
    } else {
      if (e.currentTarget.textContent === "Stop") {
        this.setState({ test: false });
        correctAdd = [];
        incorrectAdd = [];
        // replays number said
      } else {
        speakDe(this.state.rand, lang);

      }
    }
  }
  //start challenge button
  handleQuizButtonClick(e) {
    let lang = this.state.lang;
    let rand = this.state.theNum[Math.floor((Math.random() * this.state.theNum.length))][0];
    //resets numbers to remove correct class
    let theNewNum = [];
    for (i = this.state.from; i <= this.state.to; i++) {
      theNewNum.push([i, 'num']);
    }

    // starts challenge,
    if (this.state.quiz !== true) {
      this.setState({
        rand: rand,
        quiz: true,
        finished: false,
        incorrectQ: [],
        elapsedTime: 0,
        running: true,
        previousTime: Date.now(),
        theNum: theNewNum
      });
      speakDe(rand, lang);

      // stops challenge, resets incorrectAdd arrays
    } else {
      if (e.currentTarget.textContent === "Stop") {
        this.setState({ quiz: false, running: false });
        incorrectAdd = [];
        // replays number said
      } else {
        speakDe(this.state.rand, lang);
      }
    }
  }

  //for timer
  componentDidMount() {
    this.interval = setInterval(this.onTick, 100);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  onTick() {
    if (this.state.running) {
      var now = Date.now();
      this.setState({
        previousTime: now,
        elapsedTime: this.state.elapsedTime + (now - this.state.previousTime)
      })
    }
  }

  render() {
    let theNum = this.state.theNum;
    let correct = this.state.correct;
    let incorrect = this.state.incorrect;
    let incorrectQ = this.state.incorrectQ;
    let showTest = null;
    let showQuiz = null;
    let showTest2 = null;

    let timer = Math.floor(this.state.elapsedTime);
    let mins = Math.floor(timer / 60000);
    let secs = ((timer % 60000) / 1000).toFixed(0);
    let theTime = (secs === 60 ? (mins + 1) + ":00" : mins + ":" + (secs < 10 ? "0" : "") + secs);

    if (this.state.quiz !== true && this.state.finished === false) {
      showTest = <PracticeBlock
        toShow={this.state.test}
        onClick={this.handleButtonClick}
        correct={correct}
        incorrect={incorrect}
      />
    }
    if (this.state.test !== true) {
      showQuiz = <ChallengeBlock
        toShow={this.state.quiz}
        onClick={this.handleQuizButtonClick}
        incorrect={incorrectQ}
        timer={theTime}

        finished={this.state.finished}
        results={this.state.results}
      />
    }
    if (this.state.finished === true) {
      showTest2 = <PracticeBlock
        toShow={this.state.test}
        onClick={this.handleButtonClick}
        correct={correct}
        incorrect={incorrect}
      />
    }

    return (
      <div className="App container">
        <HeaderCount langSelected={this.state.langSelected} langs={langs} />

        <LangSelect onChange={this.handleChange} langs={langs} defaultLang={this.state.lang} />

        <RangeSelect
          onChangeFrom={this.handleFromChange}
          onChangeTo={this.handleToChange}
          fromList={fromList}
          toList={this.state.toList}
          defaultFrom={this.state.from}
          defaultTo={this.state.to}
        />

        <NumbersDisplay theNum={theNum} onClick={this.handleClick} numClass={'num'} />
        <ReactCSSTransitionGroup
          transitionName="slide"
          transitionEnterTimeout={600}
          transitionLeaveTimeout={600}
        >
          {showTest}

          {showQuiz}
          {showTest2}
        </ReactCSSTransitionGroup>

      </div>
    );
  }
}

export default App;
