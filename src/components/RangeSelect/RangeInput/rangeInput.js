import React from 'react';

export const RangeInput = (props) => {
  return (

    <div className="form-inline">
      <label htmlFor={props.label}>{props.label}:
        </label>
      <select
        className="form-control"
        id={props.label}
        onChange={props.onChange}
        value={props.defaultStart}
      >

        {props.theList.map((theStart, index) => <option key={index} value={theStart}>
          {theStart}
        </option>)
        }
      </select>
    </div>
  );
}
