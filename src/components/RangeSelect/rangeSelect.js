import React from 'react';
import { RangeInput } from './RangeInput/rangeInput';

export const RangeSelect = (props) => {
  return (
    <div className="RangeSelect">
      <div className="form-inline">
        <RangeInput
          onChange={props.onChangeFrom}
          theList={props.fromList}
          defaultStart={props.defaultFrom} //
          label={"From"} />

        <RangeInput
          onChange={props.onChangeTo}
          theList={props.toList}
          defaultStart={props.defaultTo}
          label={"To"} />
      </div>
    </div>
  );
}
