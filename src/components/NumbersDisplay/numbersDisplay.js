import React from 'react';
import './NumbersDisplay.css';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

export const NumbersDisplay = (props) => {
  let theNum = props.theNum;

  return (

    <ReactCSSTransitionGroup
      component="ul"
      transitionName="sn"
      className="numbers-display"
      transitionEnterTimeout={600}
      transitionLeaveTimeout={600}
      transitionAppear={true}
      transitionAppearTimeout={600}
    >
      {theNum.map((theNum, index) => <li className={theNum[1]} key={index} onClick={props.onClick}>
        {theNum[0]}
      </li>)}

    </ReactCSSTransitionGroup>

  );
}
