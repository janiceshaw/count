import React from 'react'

export const Buttons = (props) => {
  if (props.toShow === true) {
    return (
      <div>
        <button type='button' className='stop btn' onClick={props.onClick}>Stop</button>
        <button type='button' className='repeat btn' onClick={props.onClick}>Repeat</button>
      </div>
    )
  } else {
    return (
      <div>
        <button type='button' className='play btn' onClick={props.onClick}>Play</button>
      </div>

    )
  }
}
