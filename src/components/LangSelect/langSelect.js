import React from 'react';

export const LangSelect = (props) => {
  return (
    <div className="LangSelect">
      <div className="form-inline">
        <label htmlFor="lang">Change language
        </label>
        <select defaultValue={props.defaultLang} className="form-control" id="lang" onChange={props.onChange}>

          {props.langs.map((langs, index) => <option className={langs[0]} key={index} value={langs[1]}>
            {langs[0]}
          </option>)
          }
        </select>
      </div>
    </div>
  );
}
