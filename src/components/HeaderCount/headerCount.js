import React from 'react';

export const HeaderCount = (props) => {
  let lang = props.langSelected;
  return (
    <header>
      <h1>Count <span className='in'>In</span></h1>
      <h2>{props.langs[lang][0]} / <span style={{ color: 'white' }}>{props.langs[lang][2]}</span>
      </h2>
      {/* <p>Click the numbers to hear them in your chosen language.</p> */}
    </header>
  );
}

